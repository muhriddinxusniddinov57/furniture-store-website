import styled from 'styled-components'
import React, {useRef, useEffect, useCallback} from 'react';
import {MdClose} from "react-icons/md";
import {AiFillCamera} from "react-icons/ai"


const Backgound = styled.div`
width:950px;
height: 650px;
border-radius: 16px;
border: none;
background-color: white;
margin-left: 250px;
margin-top: 120px;
position: fixed;
position: relative;
overflow: auto;


`
const CloseModalBtn = styled(MdClose)`
position: absolute;
width: 35px;
height: 35px;
top:10px;
right: 10px;
`




const Container = styled.div`
width: 1515px;
height: 915px;
border: 1px solid black;
background:rgba(0,0,0,0.7);
margin-top: 700px;
position: fixed;
margin-left:-
15px;

`

const Nav = styled.div`
width: 950px;
height: 60px;
border:1px solid black;
border: none;
position: fixed;
z-index: 1000000000;
background-color: #ffff;
color: silver;
        

`

const Bloock = styled.div`
width: 950px;
height: 850px;
border: none;
display: flex;
flex-wrap: wrap;
margin-top: 20px;
`
const Box = styled.div`
border: none;
width: 200px;
height: 250px;
margin: 0 16px;
border-radius: 10px;
transition: 0.3s;
max-width: 100%;
margin-top: 15px;

:hover{

    transform: scale(1.1);
   

}

`
const BoxImg = styled.img`
width: 200px;
height: 200px;
border: none;
transform: scale(1);
transition: 0.5s ease-in-out;
  /* :hover{
  transform: scale(1.2);
  animation-duration: 1s;

  } */
`
const Figure = styled.figure`
overflow: hidden;
height: 200px;
width: 200px;

`


const BoxFooter = styled.div`
width: 200px;
height: 50px;
border:none;
display: flex;
align-items: center;
margin-top: -16px;
:hover{
background:rgba(0,0,0,0.3);

}
`

const BoxValue = styled.p`
font-size: 18px;
margin-top: 13px;
margin-left: 10px;
position: relative;
::after{
  content: "";
  width: 100%;
  height: 1px;
  background: black;
  position: absolute;
  bottom: 13px;
  left: 0;

} 

`
const BoxValueOrg = styled.p`
font-size: 22px;
margin-top: 10px;
margin-left: 7px;
`

const BoxButton = styled.button`
width: 70px;
height: 27px;
border-radius: 10px;
border: none;
text-align: center;
color: white;
margin-left: 30px;
align-items: center;
`
const H3 = styled.h3`
display: flex;
justify-content: center;
align-items: center;
margin-top: 10px;
`


export const Modal = ({showModal, setShowModal})=>{


    return(
        <>
        {showModal ? <Container>
        <Backgound className='shadow'>
            <Nav className='shadow'>    <CloseModalBtn aria-label='Close-modal' onClick={()=>setShowModal((prev=>!prev))} />
            <H3>Choose your favorite furniture</H3>
            </Nav>
                <Bloock>
                    <Box className='shadow'>
                        <Figure>
                        <BoxImg src='./image/fur10.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter>
                    </Box>
                    <Box className='shadow'>
                    <Figure>
                        <BoxImg src='./image/furniture1.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter>
                    </Box>
                    <Box className='shadow'>
                    <Figure>
                        <BoxImg src='./image/fur2.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter>
                    </Box>
                    <Box className='shadow'><Figure>
                        <BoxImg src='./image/fur3.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter></Box>
                    <Box className='shadow'><Figure>
                        <BoxImg src='./image/fur4.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter></Box>
                    <Box className='shadow'><Figure>
                        <BoxImg src='./image/fur5.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter></Box>
                    <Box className='shadow'><Figure>
                        <BoxImg src='./image/fur6.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter></Box>
                    <Box className='shadow'><Figure>
                        <BoxImg src='./image/fur7.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter></Box>
                    <Box className='shadow'><Figure>
                        <BoxImg src='./image/fur10.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter></Box>
                    <Box className='shadow'><Figure>
                        <BoxImg src='./image/fur9.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter></Box>
                    <Box className='shadow'><Figure>
                        <BoxImg src='./image/fur2.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter></Box>
                    <Box className='shadow'><Figure>
                        <BoxImg src='./image/fur3.jpg' />
                        </Figure>
                            <BoxFooter>
                            <BoxValue>120$</BoxValue>
                            <BoxValueOrg>75$</BoxValueOrg>
                            <BoxButton className='btn-primary'>Buy</BoxButton>
                        </BoxFooter></Box>
                </Bloock>
       
        </Backgound>
        </Container>  : null}
        </>
    )
}


